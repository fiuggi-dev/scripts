#!/bin/bash

## TODO: add the installation direcotry at line 7
## Note: this script will install llvm-9 from scratch.
## If llvm has already existed, put the llvm-project in fcc-workspace

install_dir=

mkdir -p ${install_dir}/fcc-workspace
cd ${install_dir}/fcc-workspace

git clone https://ds01210@bitbucket.org/fiuggi-dev/fcc.git
git clone --single-branch --branch release/9.x https://github.com/llvm/llvm-project.git

cd llvm-project
git apply ../fcc/bootstrap/diff-llvm-on-glibc231.patch
cd -

cd fcc/bootstrap

sed -i "s:install-prefix=.*:install-prefix=${install_dir}\/fcc-workspace:" Makefile
sed -i "s:fcc-root-path=.*:fcc-root-path=${install_dir}\/fcc-workspace\/fcc:" Makefile

make all
